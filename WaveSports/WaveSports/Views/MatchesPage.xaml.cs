using System.Linq;
using System.Threading.Tasks;
using WaveSports.Models;
using WaveSports.Repository;
using WaveSports.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WaveSports.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MatchesPage : ContentPage
	{
		private ConstantData _constantData;
		private ApiService _apiService;

		private string codeLeague;

		public MatchesPage(string code)
		{
			_constantData = new ConstantData();

			_apiService = new ApiService();

			codeLeague = code;

			InitializeComponent();
		}

		protected override async void OnAppearing()
		{
			IsLoading.IsRunning = true;

			IsLoading.IsVisible = true;

			await GetDataAsync();


			base.OnAppearing();
		}

		public async Task GetDataAsync()
		{
			var data = await _apiService.GetMatchDataAsync<MatchesModel>(codeLeague, 7);
			

			if (data != null)
			{
				matchView.ItemsSource = data.matches.ToList();

				IsLoading.IsRunning = false;
				IsLoading.IsVisible = false;
			}
			else
			{
				IsLoading.IsRunning = false;
				IsLoading.IsVisible = false;
			}
		}

		private async void matchView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var matchId = (Matches)e.SelectedItem;

			await Navigation.PushAsync(new MatchDetailsPage(matchId));
		}
	}
}
