using System;
using System.Collections.Generic;
using WaveSports.Models.CommonModels;

namespace WaveSports.Models
{
	public class Area
	{
		public int id { get; set; }
		public string name { get; set; }
	}

	public class Competition
	{
		public int id { get; set; }
		public Area area { get; set; }
		public string name { get; set; }
		public string code { get; set; }
		public string plan { get; set; }
		public DateTime lastUpdated { get; set; }
	}

	public class Season
	{
		public int id { get; set; }
		public string startDate { get; set; }
		public string endDate { get; set; }
		public int currentMatchday { get; set; }
	}

	public class Table
	{
		public int position { get; set; }
		public Team team { get; set; }
		public int playedGames { get; set; }
		public int won { get; set; }
		public int draw { get; set; }
		public int lost { get; set; }
		public int points { get; set; }
		public int goalsFor { get; set; }
		public int goalsAgainst { get; set; }
		public int goalDifference { get; set; }
	}

	public class TableHeader
	{
		public string position { get; set; }
		public string team { get; set; }
		public string playedGames { get; set; }
		public string won { get; set; }
		public string draw { get; set; }
		public string lost { get; set; }
		public string points { get; set; }

		public string goals { get; set; }
	}
	public class Standing
	{
		public string stage { get; set; }
		public string type { get; set; }
		public object group { get; set; }
		public List<Table> table { get; set; }
	}

	public class StandingModel
	{
		public Competition competition { get; set; }
		public Season season { get; set; }
		public List<Standing> standings { get; set; }
	}
}
