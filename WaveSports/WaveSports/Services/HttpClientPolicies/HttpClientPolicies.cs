using Polly;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace WaveSports.Services.HttpClientPolicy
{
	public class HttpClientPolicies
	{
		public Task<TResult> ExecuteAsync<TResult>(Func<Task<TResult>> func)
		{
			return Policy.Handle<HttpRequestException>(ex =>
			{
				Debug.WriteLine("Exception", ex.Message);
				return true;
			}).WaitAndRetryAsync
			(
				5,
				retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))

			).ExecuteAsync<TResult>(func);
		}
	}
}
