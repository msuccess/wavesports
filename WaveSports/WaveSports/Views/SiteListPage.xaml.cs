using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WaveSports.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SiteListPage : ContentPage
	{
		public SiteListPage()
		{
			InitializeComponent();
		}

		private async void BbcButton_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new HeadlinesPage("bbc-sport", "BBC Sport Headlines")).ConfigureAwait(false);
		}

		private async void BlButton_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new HeadlinesPage("bleacher-report", "Bleacher Report Headlines")).ConfigureAwait(false);
		}

		private async void EspnButton_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new HeadlinesPage("espn", "ESPN Headlines")).ConfigureAwait(false);
		}

		private async void FourButton_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new HeadlinesPage("four-four-two", "FourFourTwo Headlines")).ConfigureAwait(false);
		}

		private async void FoxButton_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new HeadlinesPage("fox-sports", "Fox Sports Headlines")).ConfigureAwait(false);
		}

		private async void SptBlButton_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new HeadlinesPage("the-sport-bible", "The Sport Bible Headlines")).ConfigureAwait(false);
		}

		private async void FootButton_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new HeadlinesPage("football-italia", "Football Italia Headlines")).ConfigureAwait(false);
		}

		private async void TalkButton_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new HeadlinesPage("talksport", "TalkSport Headlines")).ConfigureAwait(false);
		}
	}
}
