using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using WaveSports.Models;

namespace WaveSports.Repository
{
	public class ApiService
	{
		private readonly Constants _constant;

		private HttpClient _httpClient;

		public List<MasterPageItem> menuList { get; set; }

		public ApiService()
		{
			_constant = new Constants();

			_httpClient = new HttpClient();
		}

		public async Task<T> GetNewsDataAsync<T>(string newsUrl)
		{
			try
			{
				var url = _constant.Url + newsUrl + _constant.ApiKey;

				var response = await _httpClient.GetAsync(url);

				if (response.IsSuccessStatusCode)
				{
					string content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

					var newsdata = JsonConvert.DeserializeObject<T>(content);

					Console.WriteLine(newsdata);

					return newsdata;
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine("Exception", e.Message);

				throw new HttpRequestException(e.ToString());
			}

			return default(T);
		}

		public async Task<T> GetLeagueDataAsync<T>(string leagueCode)
		{
			try
			{
				var url = _constant.StatsUrl + leagueCode + "/standings";

				_httpClient.DefaultRequestHeaders.Add("X-Auth-Token", _constant.ApiKeySocers);

				var response = await _httpClient.GetAsync(url);

				if (response.IsSuccessStatusCode)
				{
					string content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

					var dataLeague = JsonConvert.DeserializeObject<T>(content);

					Debug.WriteLine(dataLeague);

					return dataLeague;
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine("Exception", e.Message);

				throw new HttpRequestException(e.ToString());
			}

			return default(T);
		}

		public async Task<T> GetMatchDataAsync<T>(string leagueCode, int matchday)
		{
			try
			{
				var url = _constant.StatsUrl + leagueCode + "/matches?matchday=" + matchday;

				_httpClient.DefaultRequestHeaders.Add("X-Auth-Token", _constant.ApiKeySocers);

				var response = await _httpClient.GetAsync(url);

				if (response.IsSuccessStatusCode)
				{
					string content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

					var dataMatches = JsonConvert.DeserializeObject<T>(content);

					Debug.WriteLine(dataMatches);

					return dataMatches;
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine("Exception", e.Message);

				throw new HttpRequestException(e.ToString());
			}

			return default(T);
		}

		public async Task<T> GetRecentMatchAsync<T>()
		{
			try
			{
				var url = "https://api.football-data.org/v2/matches";

				_httpClient.DefaultRequestHeaders.Add("X-Auth-Token", _constant.ApiKeySocers);

				var response = await _httpClient.GetAsync(url);

				if (response.IsSuccessStatusCode)
				{
					string content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

					var dataMatches = JsonConvert.DeserializeObject<T>(content);

					Debug.WriteLine(dataMatches);

					return dataMatches;
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine("Exception", e.Message);

				throw new HttpRequestException(e.ToString());
			}

			return default(T);
		}
		//private HttpClient CreateHttpClient()
		//{
		//	var httpClient = new HttpClient();

		//	httpClient.DefaultRequestHeaders.Accept
		//		.Add(new MediaTypeWithQualityHeaderValue("application/json"));

		//	httpClient.DefaultRequestHeaders.Authorization =
		//		new AuthenticationHeaderValue("X-Auth-Toke", _constant.ApiKeySocers);

		//	return httpClient;
		//}
	}
}
