namespace WaveSports.Models.CommonModels
{
	public class League
	{
		public string name { get; set; }
		public string code { get; set; }
		public string imageUrl { get; set; }
	}
}
