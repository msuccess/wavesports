using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace WaveSports
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();

			MainPage = new MainPage();
		}

		//private async Task InitializeNavigation()
		//{
		//	var navigationService = AppContainer.Resolve<INavigationService>();
		//	await navigationService.InitializeAsync();
		//}

		//private void InitializeApp()
		//{
		//	AppContainer.RegisterDependencies();

		//	var shoppingCartViewModel = AppContainer.Resolve<>();
		//	shoppingCartViewModel.InitializeMessenger();
		//}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
