namespace WaveSports.Models.CommonModels
{
	public class Area
	{
		public int id { get; set; }
		public string name { get; set; }
	}
}
