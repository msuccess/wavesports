using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WaveSports.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LeagueMainPage : TabbedPage
	{
		public string titleText;

		public LeagueMainPage(string name, string code)
		{
			InitializeComponent();

			Children.Add(new StandingsPage(code));

			Children.Add(new ScoresPage(code));

			Children.Add(new MatchesPage(code));

			BindingContext = this;

			titleText = name;

			DisplayAlert("Info", "", "OK");
		}

		protected override void OnAppearing()
		{
			Title = titleText;

			base.OnAppearing();
		}
	}
}
