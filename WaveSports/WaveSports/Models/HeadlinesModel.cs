using System.Collections.Generic;
using System.Linq;

namespace WaveSports.Models
{
	public class HeadlinesModel
	{
		public string ImageUrl { get; set; }

		public List<HeadlinesModel> HeadlinesImages()
		{
			var headlinesModel = new List<HeadlinesModel>
			{
				new HeadlinesModel()
				{
					ImageUrl="https://en.wikipedia.org/wiki/Fox_Sports_Networks#/media/File:Fox_Sports_Networks_logo.png"
				},
				new HeadlinesModel()
				{
					ImageUrl=""
				}
			};
			return headlinesModel.ToList();
		}
	}
}
