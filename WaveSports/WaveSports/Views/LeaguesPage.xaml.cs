using WaveSports.Models;
using WaveSports.Models.CommonModels;
using WaveSports.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WaveSports.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LeaguesPage : ContentPage
	{
		private Constants _constant;

		private ConstantData _constantData;

		public LeaguesPage()
		{
			_constant = new Constants();

			_constantData = new ConstantData();

			InitializeComponent();
		}

		protected override void OnAppearing()
		{
			IsLoading.IsRunning = true;

			ListLeague.ItemsSource = _constantData.GetLeagues();

			IsLoading.IsRunning = false;
			IsLoading.IsVisible = false;

			base.OnAppearing();
		}

		private async void ListLeague_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var content = e.SelectedItem as League;

			await Navigation.PushAsync(new LeagueMainPage(content.name, content.code));
		}
	}
}
