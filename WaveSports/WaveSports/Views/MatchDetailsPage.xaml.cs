using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaveSports.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WaveSports.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MatchDetailsPage : ContentPage
	{
		public MatchDetailsPage (Matches id)
		{
			InitializeComponent ();

			DisplayAlert("Success", id.ToString(), "OK");
		}
		//https://api.football-data.org/v2/matches/205030
	}
}
