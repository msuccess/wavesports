using System;
using System.Linq;
using System.Threading.Tasks;
using WaveSports.Models;
using WaveSports.Repository;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WaveSports.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HeadlinesPage : ContentPage
	{
		private readonly ApiService apiService;

		public string siteUrl;

		public string titleText;

		public HeadlinesPage(string url, string siteTitle)
		{
			BindingContext = this;

			InitializeComponent();

			siteUrl = url;

			titleText = siteTitle;

			apiService = new ApiService();
		}

		protected override async void OnAppearing()
		{
			Title = titleText;

			IsLoading.IsRunning = true;

			await GetDataAsync().ConfigureAwait(false);

			base.OnAppearing();
		}

		public async Task GetDataAsync()
		{
			var data = await apiService.GetNewsDataAsync<NewsModel>(siteUrl);
			if (data != null)
			{
				newsView.ItemsSource = data.articles.ToList();

				IsLoading.IsRunning = false;
				IsLoading.IsVisible = false;
			}
			else
			{
				await DisplayAlert("Error", "Check Internet Connection", "OK");
			}
		}

		private void newsView_ItemTapped(object sender, ItemTappedEventArgs e)
		{
			var content = e.Item as Article;
		}

		private void Button_Clicked(object sender, EventArgs e)
		{
			var url = (sender as Button)?.CommandParameter.ToString();

			Device.OpenUri(new Uri(url));
		}

		private async void NewsView_Refreshing(object sender, EventArgs e)
		{
			await GetDataAsync().ConfigureAwait(false);

			newsView.EndRefresh();
		}
	}
}
