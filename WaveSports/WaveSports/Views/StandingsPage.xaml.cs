using System.Threading.Tasks;
using WaveSports.Models;
using WaveSports.Repository;
using WaveSports.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WaveSports.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StandingsPage : ContentPage
	{
		private ConstantData _constantData;

		private ApiService _apiService;

		public string codeLeague;

		public StandingsPage(string code)
		{
			_constantData = new ConstantData();

			_apiService = new ApiService();

			InitializeComponent();

			codeLeague = code;
		}

		protected override async void OnAppearing()
		{
			IsLoading.IsRunning = true;
			IsLoading.IsVisible = true;

			await GetDataAsync();

			base.OnAppearing();
		}

		public async Task GetDataAsync()
		{
			var data = await _apiService.GetLeagueDataAsync<StandingModel>(codeLeague);

			if (data != null)
			{
				standingView.ItemsSource = data.standings
							.Find(a => a.type == "TOTAL").table;

				IsLoading.IsRunning = false;
				IsLoading.IsVisible = false;
			}
		}
	}
}
