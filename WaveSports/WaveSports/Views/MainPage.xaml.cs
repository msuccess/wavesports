using System;
using WaveSports.Models;
using WaveSports.Services;
using WaveSports.Views;
using Xamarin.Forms;

namespace WaveSports
{
	public partial class MainPage : MasterDetailPage
	{
		private ConstantData _constantData;

		public MainPage()
		{
			InitializeComponent();
			_constantData = new ConstantData();

			SideMenuList.ItemsSource = _constantData.MenuList();

			Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(SiteListPage)));
		}

		private void SideMenuList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var menu = e.SelectedItem as MasterPageItem;

			Type page = menu.TargetType;

			Detail = new NavigationPage((Page)Activator.CreateInstance(page));

			IsPresented = false;
		}
	}
}
