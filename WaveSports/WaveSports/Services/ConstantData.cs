using System.Collections.Generic;
using System.Linq;
using WaveSports.Models;
using WaveSports.Models.CommonModels;
using WaveSports.Views;

namespace WaveSports.Services
{
	public class ConstantData
	{
		private List<MasterPageItem> menuList;

		private List<TableHeader> standingList;

		public List<League> GetLeagues()
		{
			var leagues = new List<League>
			{
				new League
				{
				name= "Premier League",
				code= "PL",
				imageUrl="https://upload.wikimedia.org/wikipedia/en/thumb/f/f2/Premier_League_Logo.svg/205px-Premier_League_Logo.svg.png"
				},
				new League
				{
				name= "Bundesliga",
				code= "BL1",
				imageUrl="https://upload.wikimedia.org/wikipedia/en/thumb/3/35/Bundesliga_picture_logo.svg/336px-Bundesliga_picture_logo.svg.png"
				},
				new League
				{
				name= "Ligue 1",
				code= "FL1",
				imageUrl="https://upload.wikimedia.org/wikipedia/en/thumb/d/dd/Ligue_1_Logo.svg/348px-Ligue_1_Logo.svg.png"
				},
				new League
				{
				name= "Serie A",
				code= "SA",
				imageUrl="https://upload.wikimedia.org/wikipedia/en/f/f7/LegaSerieAlogoTIM.png"
				}
			};

			return leagues.ToList();
		}

		public List<MasterPageItem> MenuList()
		{
			menuList = new List<MasterPageItem>
			{
				new MasterPageItem()
				{
					Title = "News",
					Icon = "news.png",
					TargetType = typeof(SiteListPage)
				},
				new MasterPageItem()
				{
					Title = "Leagues",
					Icon = "league.png",
					TargetType = typeof(LeaguesPage)
				},
				new MasterPageItem()
				{
					Title = "Matches Today",
					Icon = "match.png",
					TargetType = typeof(RecentMatchPage)
				},
				new MasterPageItem()
				{
					Title = "Setting",
					Icon = "gears.png",
					TargetType = typeof(SettingsPage)
				}
			};
			return menuList.ToList();
		}

		public List<TableHeader> StandingList()
		{
			standingList = new List<TableHeader>
			{
				new TableHeader()
				{
					position = "#",
					team="Team",
					playedGames="P",
					won="W",
					draw="D",
					lost="L",
					points="PTS",
					goals="Goals",
				}
			};
			return standingList.ToList();
		}
	}
}
