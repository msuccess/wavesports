namespace WaveSports.Models
{
	public class Constants
	{
		public string StatsUrl = "https://api.football-data.org/v2/competitions/";

		public string ApiKey = "&apiKey=092e614dcac24ab7a034328cf16d64da";

		public string ApiKeySocers = "d7e5dacec2ea46b99d15535a39de9a8a";

		public string Url = "https://newsapi.org/v2/top-headlines?sources=";

		public string EplCode = "PL";

		public string BundesligaCode = "BL1";

		public string Ligue1Code = "FL1";

		public string SerieACode = "SA";

		public string RecentMatches = "https://api.football-data.org/v2/matches";
	}
}
